// Import express
let express = require('express');
// Import Body parser
let bodyParser = require('body-parser');

// Import Mongoose
const mongoose = require('mongoose');
AutoIncrement = require('mongoose-sequence')(mongoose);

// Connect to Mongoose and set connection variable
mongoose.connect('mongodb://localhost/feed', { useNewUrlParser: true, useUnifiedTopology: true});
var db = mongoose.connection;

// Initialize the app
let app = express();


// Configure bodyparser to handle post requests
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

// Added check for DB connection

if(!db)
    console.log("Error connecting db")
else
    console.log("Db connected successfully")

// Setup server port
var port = process.env.PORT || 8080;

// Send message for default URL
app.get('/', (req, res) => res.send('Hello World with Express'));

// Import routes
let apiRoutes = require("./api-routes");
app.use('/api', apiRoutes);

// Launch app to listen to specified port
app.listen(port, function () {
    console.log("Running API on port " + port);
});
