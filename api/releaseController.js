// releaseController.js
// Import release model
Release = require('./releaseModel');
// Handle index actions
exports.index = function (req, res) {
    Release.get(function (err, releases) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "Releases retrieved successfully",
            data: releases
        });
    });
};
// Handle create release actions
exports.new = function (req, res) {
    var release = new Release();
    release.name = req.body.name ? req.body.name : release.name;
    release.thumbnail = req.body.thumbnail;
// save the release and check for errors
    release.save(function (err) {
        // Check for validation error
        if (err)
            res.json(err);
        else
            res.json({
                message: 'New release created!',
                data: release
            });
    });
};
// Handle view release info
exports.view = function (req, res) {
    Release.findById(req.params.release_id, function (err, release) {
        if (err)
            res.send(err);
        res.json({
            message: 'Release details loading..',
            data: release
        });
    });
};
// Handle update release info
exports.update = function (req, res) {
    Release.findById(req.params.release_id, function (err, release) {
        if (err)
            res.send(err);
        release.name = req.body.name ? req.body.name : release.name;
        release.thumbnail = req.body.thumbnail ? req.body.thumbnail : release.thumbnail;
// save the release and check for errors
        release.save(function (err) {
            if (err)
                res.json(err);
            res.json({
                message: 'Release Info updated',
                data: release
            });
        });
    });
};
// Handle delete release
exports.delete = function (req, res) {
    Release.remove({
        _id: req.params.release_id
    }, function (err, release) {
        if (err)
            res.send(err);
        res.json({
            status: "success",
            message: 'Release deleted'
        });
    });
};
