// artistModel.js
var mongoose = require('mongoose');

// Setup schema
var ArtistSchema = mongoose.Schema({
    _id : {
        type: Number
    },
    name: {
        type: String,
        required: true
    },
    thumbnail: {
        type: String
    }
}, { _id: false });

ArtistSchema.plugin(AutoIncrement, {inc_field: '_id'});

// Export Artist model
var Artist = module.exports = mongoose.model('Artist', ArtistSchema);

module.exports.get = function (callback, limit) {
    Artist.find(callback).limit(limit);
}

module.exports.search = function (query, callback, limit) {
    Artist.find({name: {$regex: query, $options: 'i'}}, callback).limit(limit)
}