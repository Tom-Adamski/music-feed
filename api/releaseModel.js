// releaseModel.js
var mongoose = require('mongoose');

// Setup schema
var ReleaseSchema = mongoose.Schema({
    _id : {
        type: Number
    },
    artistId : {
        type: Number,
        required: true
    },
    title: {
        type: String,
        required: true
    },
    type: {
        type: String
    },
    thumbnail: {
        type: String
    },
    date_out: {
        type: Date,
        required: true
    },
    date_add: {
        type: Date,
        default: Date.now
    },
    date_mod: {
        type: Date,
        default: Date.now
    }
}, { _id: false });

ReleaseSchema.plugin(AutoIncrement, {inc_field: '_id'});

// Export Release model
var Release = module.exports = mongoose.model('Release', ReleaseSchema);

module.exports.get = function (callback, limit) {
    Release.find(callback).limit(limit);
}
