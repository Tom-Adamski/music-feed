// artistController.js
// Import artist model
Artist = require('./artistModel');
// Handle index actions
exports.index = function (req, res) {
    Artist.get(function (err, artists) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "Artists retrieved successfully",
            data: artists
        });
    });
};
// Handle create artist actions
exports.new = function (req, res) {
    var artist = new Artist();
    artist.name = req.body.name ? req.body.name : artist.name;
    artist.thumbnail = req.body.thumbnail;
// save the artist and check for errors
    artist.save(function (err) {
        // Check for validation error
        if (err)
            res.json(err);
        else
            res.json({
                message: 'New artist created!',
                data: artist
            });
    });
};
// Handle view artist info
exports.view = function (req, res) {
    Artist.findById(req.params.artist_id, function (err, artist) {
        if (err)
            res.send(err);
        res.json({
            message: 'Artist details loading..',
            data: artist
        });
    });
};
// Handle update artist info
exports.update = function (req, res) {
    Artist.findById(req.params.artist_id, function (err, artist) {
        if (err)
            res.send(err);
        artist.name = req.body.name ? req.body.name : artist.name;
        artist.thumbnail = req.body.thumbnail ? req.body.thumbnail : artist.thumbnail;
// save the artist and check for errors
        artist.save(function (err) {
            if (err)
                res.json(err);
            res.json({
                message: 'Artist Info updated',
                data: artist
            });
        });
    });
};
// Handle delete artist
exports.delete = function (req, res) {
    Artist.remove({
        _id: req.params.artist_id
    }, function (err, artist) {
        if (err)
            res.send(err);
        res.json({
            status: "success",
            message: 'Artist deleted'
        });
    });
};
// Handle search artist
exports.search = function (req, res) {
    Artist.search(req.params.artist_query, function (err, artists) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "Artists retrieved successfully",
            data: artists
        });
    });
};