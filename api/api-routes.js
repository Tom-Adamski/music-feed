// api-routes.js
// Initialize express router
let router = require('express').Router();
// Set default API response
router.get('/', function (req, res) {
    res.json({
        status: 'API Its Working',
        message: 'Welcome to RESTHub crafted with love!',
    });
});
// Import artist controller
var artistController = require('./artistController');
// Artist routes
router.route('/artists')
    .get(artistController.index)
    .post(artistController.new);

router.route('/artists/search/:artist_query')
    .get(artistController.search);

router.route('/artists/:artist_id')
    .get(artistController.view)
    .patch(artistController.update)
    .put(artistController.update)
    .delete(artistController.delete);

// Import release controller
var releaseController = require('./releaseController');
// Artist routes
router.route('/releases')
    .get(releaseController.index)
    .post(releaseController.new);

router.route('/releases/search/:release_query')
    .get(releaseController.search);

router.route('/releases/:release_id')
    .get(releaseController.view)
    .patch(releaseController.update)
    .put(releaseController.update)
    .delete(releaseController.delete);


// Export API routes
module.exports = router;